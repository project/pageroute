<?php 

/**
 * @file
 * Page view type
 */


include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page.inc');

class pageroute_page_view extends pageroute_page {  
  /*
   * Returns the page display for the configured node
   */
  public function get_form(&$form_state, &$args) {
  
    $form = array();
    
    if (!isset($this->options['nid'])) {
      $this->options['nid'] = pageroute_page_get_nid($this);
    }
    
    if ($this->options['nid'] == 0) {
      $this->options['nid'] = $args['nid'];
    }
    
    if ($this->options['nid']) {    
      
      $node = node_load($this->options['nid']);
      if ($node->nid && node_access('view', $node)) {
        if (empty($this->title)) {
          drupal_set_title(check_plain($node->title));
        }
        node_tag_new($node->nid);
        $form['page_form'] = array('#value' => node_view($node, FALSE, TRUE, FALSE));
      
        return $form;
      }
      else if (db_result(db_query('SELECT nid FROM {node} WHERE nid = %d', $this->options['nid']))) {
        drupal_access_denied();
        pageroute_exit_now();
      }
    }
    drupal_not_found();
    pageroute_exit_now();
  }
  
  public static function ui($page, &$form) {
    $form['options']['nid'] = array(
      '#type' => 'textfield',
      '#title' => t('Node ID'),
      '#description' => t('Enter the node ID of the node that should be '.
                          'displayed at this page. Enter 0 to use the '.
                          'first argument as node ID like other node '.
                          'page type does.'),
      '#required' => TRUE,
      '#default_value' => isset($page->options['nid']) ? $page->options['nid'] : '',
      '#weight' => 2,
    );
  }
  
  public static function help() {
    return t('This page type just displays a configurable node. It can also '.
             'be configured to display the node with the id taken from the '.
             'first argument. Combined with a node adding or editing form, '.
             'this enables you to build a page that shows the added or updated node.');
  }

  public static function info() {
    return array('name' => t('Node display'));
  }
  
  public function get_cancel_target() {
    return PAGEROUTE_FORWARD;
  }
  
  public function set_up() {
    include_once(drupal_get_path('module', 'node') .'/node.pages.inc');
  }
}
