<?php

/**
 * @file
 * Page user edit type
 */

include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page.inc');
include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page_add.inc');
include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page_edit.inc');

class pageroute_page_manage extends pageroute_page {  
/*
 * Shows a node management page for the given content type
 * It determines itself what has to be displayed (overview, add/edit/delete form).
 */
 
  public function get_form(&$form_state, &$args) {
    $form = array();

    switch ($args['todo']['action']) { 
      case t('Add'):
        $args['hide_pageroute_buttons'] = TRUE;      
        $args['submit_action'] = PAGEROUTE_CURRENT;        
        return pageroute_page_add::get_node_add_form(&$form_state, $this);

      case t('Edit'):
        $args['hide_pageroute_buttons'] = TRUE;      
        $args['submit_action'] = PAGEROUTE_CURRENT;        
        $args['nid'] = $args['todo']['target'];
        return pageroute_page_edit::get_node_edit_form(&$form_state, $this, $args);

      case t('Delete'):
        $args['hide_pageroute_buttons'] = TRUE;      
        $args['submit_action'] = PAGEROUTE_CURRENT;
        if (!is_numeric($args['todo']['target']) || !($node = node_load($args['todo']['target']))) {
          drupal_not_found();
          pageroute_exit_now();
        }

        return pageroute_node_delete_confirm($node, $form, $this, $args);

      default:
        $args['submit_action'] = PAGEROUTE_CURRENT;
        $args['hide_pageroute_buttons'] = FALSE;
        return pageroute_page_manage::pageroute_page_manage_overview($form_state, $this, $this->options['content-type']);
    }
  }

  /*
   * Shows the overview of node management page
   */
  public static function pageroute_page_manage_overview($form_state, $page, $content_type) {    
    $result = db_query(db_rewrite_sql("
      SELECT n.nid FROM {node} n
      WHERE n.type = '%s' AND n.uid = %d AND n.status > 0
      ORDER BY n.created
    "), $content_type, pageroute_page_get_uid($page));

    $i = 1; $output = '';
    while ($row = db_fetch_object($result)) {
      $node = node_load($row->nid);

      $buttons = array();
      
      if (node_access('update', $node)) {
        $buttons[] = t('Edit');
      }
      if (node_access('delete', $node)) {
        $buttons[] = t('Delete');
      }

      if (count($buttons)) {
        $form[$node->nid]['buttons'] = theme('pageroute_page_manage_buttons', $node, $i, $buttons);
        $buttons = drupal_render($form[$node->nid]['buttons']);
      }
      $form['node_list'][$node->nid] = array('#value' => theme('pageroute_page_manage_node', $node, $i++, $buttons));
    }
    
    //there are no nodes yet
    if ($i == 1) {
      if ($page->options['empty']['force_add']) {
        //mark that we have shown the empty add page, so that we can alter the redirect target correctly
        //and we have to apply the add form settings now
        $page->empty_add = TRUE;
        $form['target']['#value'] = PAGEROUTE_FORWARD;
        //re-add the buttons with the new settings
        unset($form['buttons']);
        pageroute_add_buttons($form, $page, 'page_op');
        return pageroute_page_add($route, $page, $form);
      }
      $types = node_get_types('names');
      $form['output'] = theme('pageroute_page_manage_empty', $types[$content_type], $page);
      $add_button = $page->options['empty']['add_button'];
    }
    else {
      $add_button = $page->options['add_button'];
      //remember all possible node ids for the target hook
      $form['node_ids'] = array('#type' => 'value', '#value' => array_keys($form['node_list']));
    }
    
    //add buttons
    if ($add_button) {
      $form['add_button'] = theme('pageroute_page_manage_add_button', $content_type, t($add_button));
    }

    return $form;
  }

  public static function ui($page, &$form) {
    $form['options']['cancel']['#access'] = FALSE;
    $form['options']['content-type'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => node_get_types('names'),
      '#required' => TRUE,
      '#default_value' => isset($page->options['content-type']) ? $page->options['content-type'] : '',
      '#weight' => 2,
    );
    $form['options']['add_button'] = array(
      '#type' => 'textfield',
      '#title' => t('Add button label'),
      '#maxlength' => 64,
      '#default_value' => isset($page->options['add_button']) ? $page->options['add_button'] : t('Add'),
      '#description' => t('The label of the Add button. Leave it empty to hide the button.'),
      '#weight' => 5,
    );
    $form['options']['empty'] = array(
      '#type' => 'fieldset', 
      '#title' => t('Overview with no nodes'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,          
      '#weight' => 6,
      '#description' => t('Configure how the page looks like, if there is no node available to list.'),
    );
    $form['options']['empty']['force_add'] = array(
      '#type' => 'radios',
      '#title' => '',
      '#default_value' => isset($page->options['empty']['force_add']) ? $page->options['empty']['force_add'] : 0,
      '#options' => array(
        0 => t('Show an add button and a short message that there is no node yet.'),
        1 => t('Show a node add form.'),
      ),
      '#weight' => 0,
    );
    $form['options']['empty']['cancel'] = array(
      '#type' => 'textfield',
      '#title' => t('Cancel link label'),
      '#maxlength' => 64,
      '#default_value' => isset($page->options['empty']['cancel']) ? $page->options['empty']['cancel'] : t('Cancel'),
      '#description' => t('If the node add form has been chosen, the label of the cancel link. Leave it empty to hide the link, but not that this would force the user to add a node.'),
      '#weight' => 4,
    );
    $form['options']['empty']['add_button'] = array(
      '#type' => 'textfield',
      '#title' => t('Add button label'),
      '#maxlength' => 64,
      '#default_value' => isset($page->options['empty']['add_button']) ? $page->options['empty']['add_button'] : t('Add'),
      '#description' => t('If shown, the label of the add button. Leave it empty to hide the button in any case.'),
      '#weight' => 4,
    );
    $groups = array(
      'add' => t('Node add form'),
      'edit' => t('Node edit form')
    );
    foreach ($groups as $name => $title) {
      $form['options'][$name] = array(
        '#type' => 'fieldset', 
        '#title' => $title,
        '#collapsed' => TRUE,
        '#collapsible' => TRUE,          
        '#weight' => 8,
      );
      $form['options'][$name]['forward'] = array(
        '#type' => 'textfield', 
        '#title' => t('Forward button label'),
        '#maxlength' => 32,
        '#default_value' => isset($page->options[$name]['forward']) ? $page->options[$name]['forward'] : t('Forward'),
        '#description' => t('The label of the forward button. Leave it empty to hide the button.'),
        '#weight' => 3,
      );
      $form['options'][$name]['back'] = array(
        '#type' => 'textfield', 
        '#title' => t('Back button label'),
        '#maxlength' => 32,
        '#default_value' => isset($page->options[$name]['back']) ? $page->options[$name]['back'] : 'Back',
        '#description' => t('The label of the back button. Leave it empty to hide the button.'),
        '#weight' => 4,
      );
      $form['options'][$name]['cancel'] = array(
        '#type' => 'textfield',
        '#title' => t('Cancel link label'),
        '#maxlength' => 32,
        '#default_value' => isset($page->options[$name]['cancel']) ? $page->options[$name]['cancel'] : t('Cancel'),
        '#description' => t('The label of the cancel link. Leave it empty to hide the link, but note that the link is the only possibility for the user to not save the form but staying in the route.'),
        '#weight' => 4,
      );
      $form['options'][$name]['preview'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display preview button'),
        '#default_value' => isset($page->options[$name]['preview']) ? $page->options[$name]['preview'] : 0,
        '#weight' => 5,        
      );
      $form['options'][$name]['submit'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display submit button'),
        '#default_value' => isset($page->options[$name]['submit']) ? $page->options[$name]['submit'] : 1,
        '#weight' => 5,        
      );
      if ($name != 'add') {
        $form['options'][$name]['nodelete'] = array(
          '#type' => 'checkbox',
          '#title' => t('Never display the delete button'),
          '#default_value' => isset($page->options[$name]['nodelete']) ? $page->options[$name]['nodelete'] : 1,
          '#weight' => 5,        
        );
        $form['options'][$name]['#weight'] = 9;
      }
    }
  }

  public static function help() {
    return t('The node management page allows one to add, edit or delete '.
             'nodes from a configurable content type from inside one page! It shows a themeable '.
             'list of already created nodes of the configured type and allows editing and deleting '.
             'if the user has the appropriate access rights.');
  }
  
  public static function info() {
    return array('name' => t('Node managment'));
  }
  
  public function get_cancel_target() {
    return PAGEROUTE_CURRENT;
  }
  
  public static function form_submitted(&$form_state) {
  
    switch ($form_state['clicked_button']['#value']) {
      case t('Preview'):
        $todo = $form_state['storage']['args']['todo'];
        break;
      case t('Delete'):
        if (is_numeric($form_state['clicked_button']['#name'])) {
          $target = $form_state['clicked_button']['#name'];
        }
        else {
          $target = $form_state['storage']['args']['todo']['target'];
        }
        $todo = array('action' => $form_state['clicked_button']['#value'], 'target' => $target);
        break;
      case t('Save'):
        unset($form_state['todo']);
        $todo = NULL;
        break;
      default:
        $todo = array('action' => $form_state['clicked_button']['#value'], 'target' => $form_state['clicked_button']['#name']);
    }

    $form_state['todo'] = $todo;
    $form_state['target'] = PAGEROUTE_CURRENT;  // stay on page, we may not get to submit (preview, error)  
  }
  
  protected function set_up() {
    include_once(drupal_get_path('module', 'node') .'/node.pages.inc');
  }  
}

/*
 * Theme the display of the edit/delete buttons of an existing node
 */
function theme_pageroute_page_manage_buttons($node, $number, $buttons) {
  
  foreach ($buttons as $key => $name) {
    $form[$name]['#attributes']['class'] = 'pageroute-'. $name;
    $form[$name]['#type'] = 'submit';
    $form[$name]['#value'] = $name;
    $form[$name]['#name'] = $node->nid;
    $form[$name]['#weight'] = $key;
  }
  $form['#prefix'] = '<span class="pageroute_manage_buttons">';
  $form['#suffix'] = '</span>';        

  return $form;
}

/*
 * Theme the display of a pageroute node management page
 */
function theme_pageroute_page_manage_node(&$node, $number, &$buttons) {

  $output = node_view($node, FALSE, TRUE, FALSE);
  $output .= $buttons;

  $title = check_plain(node_get_types('name', $node->type));
  $title .= ' '. $number .' - '. $node->title;

  $fieldset = array(
    '#title' => $title,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#children' => $output,
  );
  return theme('fieldset', $fieldset);
}


/*
 * Theme the add node button
 */
function theme_pageroute_page_manage_add_button($content_type, $label) {
  
  $form_element['#attributes']['class'] = 'pageroute-add';
  $form_element['#type'] = 'submit';
  $form_element['#name'] = $content_type;
  $form_element['#value'] = $label;
  $form_element['#prefix'] = '<span class="pageroute_manage_add_button">';
  $form_element['#suffix'] = '</span>';
  return $form_element;
}

/*
 * Themes an empty node list
 */
function theme_pageroute_page_manage_empty($type_name, $page) {
  $fieldset = array(
    '#type' => 'fieldset',
    '#title' => check_plain($type_name),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#children' => '<p>'. t('There is no @type.', array('@type' => $type_name)) .'</p>',
  );
  return $fieldset;
}
