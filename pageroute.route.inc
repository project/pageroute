<?php

/**
 * @file
 * Route
 */
 
class pageroute_route {

  var $prid;
  var $options;
  var $pages;
  var $page_index;
  var $page_access;
  var $path;

  public function pageroute_route($route_data) {  
    $this->prid = $route_data->prid;
    $this->path = $route_data->path;
    $this->options = $route_data->options;
    $this->pages = $route_data->pages;
    $this->page_index = $route_data->page_index;
    $this->page_access = $route_data->page_access;
  }

  /*
   * Load a route from the pageroute cache
   */   
  public static function load($prid, $reset = FALSE) {
    static $allroutes;
    
    if (!isset($allroutes) || $reset) {
      $allroutes = cache_get('pageroute');  
    }
        
    if ($allroutes)
      $allroutes = $allroutes->data;
    else
      $allroutes = array();
      
    if (isset($allroutes[$prid])) {
      $route_data = $allroutes[$prid];
    }
    else {
      $route_data = pageroute_route::load_from_database($prid);

      if (!$route_data) {
        return FALSE;
      }
       
      $allroutes[$prid] = $route_data;
        
      cache_set('pageroute', $allroutes);
    }      
    
    $route = new pageroute_route($route_data);
    
    drupal_alter('pageroute', $route);

    return $route;
  }

  /*
   * Load a route from the database
   */
  private static function load_from_database($prid) {
    
    $result = db_query("SELECT * FROM {pageroute_routes} WHERE prid = %d", $prid);

    $route_data = db_fetch_object($result);

    if (!$route_data) {
      return FALSE;
    }

    $route_data->options = unserialize($route_data->options);

    $result = db_query("SELECT * FROM {pageroute_pages} WHERE prid = %d ORDER BY weight, name", $route_data->prid);

    $index = 0;
    $route_data->pages = array();
    $route_data->page_index = array();
    $route_data->page_access = array();
    
    while ($page = db_fetch_object($result)) {
      $page->options = unserialize($page->options);
    
      if (isset($page->options['activated']) && !$page->options['activated']) {
        //add deactivated pages to the page_index, but don't give them an index
        //so this pages are reachable, but we won't route to them
        $route_data->page_index[$page->name] = FALSE;
        continue;
      }

      $route_data->pages[$index] = $page;
      if ($route_data->options['tabs'] && isset($page->options['no_tab']) && $page->options['no_tab']) {
        $route_data->pages[$index]->no_tab = TRUE;
      }
      $route_data->page_index[$page->name] = $index;
      $index++;
    }
    
    return $route_data;
  }

  /*
   * Get suggested page:
   * Default page if no page was specified or access denied to specified page
   * else specified page
   */
  public function get_suggested_page($page_name = NULL, $target = PAGEROUTE_CURRENT) {
    if ($page_name) {
      $page = $this->get_page($page_name, $target);
     
      if ($page) {
        if (empty($this->page_access) || $this->page_access[$page->name]) {
          $page->route = &$this;
          return $page;
        }
      }
    }

    return $this->get_default_page();
  }

  /*
   * Get default page:
   * First page that is accessable
   */
  private function get_default_page() {
    foreach ($this->pages as $page) {
      if (empty($this->page_access) || $this->page_access[$page->name]) {
        $page->route = &$this;
        return $page;
      }
    }
  }

  /*
  * Checks if this is a valid page and if access to the page shall be granted
  */
  public function check_page_access($page_name, $target = PAGEROUTE_CURRENT) {
    if ($target == PAGEROUTE_CURRENT) {
      return !isset($this->page_access[$page_name]) || $this->page_access[$page_name];
    }
    
    $access = FALSE;
    
    do {
      $page = $this->get_page($page_name, $target);
         
      if (!$page) {
        break;
      }
          
      $page_name = $page->name;
      
      $access = !isset($this->page_access[$page_name]) || $this->page_access[$page_name];
      
    } while ($access == FALSE);
    
    if ($access) {
      return $page;
    }
    
    return NULL;
  }  

  /*
   * Build and return the form for the current page
   */
  public function get_form(&$page_name, &$page, &$form_state = NULL, &$args = NULL) {  
    $page_data = $this->get_suggested_page($page_name);

    if (!$page_data) {
      return FALSE;
    }
           
    // Include needed type!
    $page = pageroute_page::get_object($page_data);
    
    if (!$page) {
      return FALSE;
    }  

    return $page->get_form($form_state, $args);
  }

  private function get_page($page_name, $target) {

    if (!isset($this->page_index[$page_name])) {
      return FALSE;
    }   

    $index = $this->page_index[$page_name];

    switch ($target) {
      case PAGEROUTE_BACK:
        return $this->pages[$index - 1];
      case PAGEROUTE_FORWARD:
        return $this->pages[$index + 1];
      case PAGEROUTE_CURRENT:
      default:
        return $this->pages[$index];
    }
  }
}
