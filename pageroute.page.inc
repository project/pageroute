<?php

/**
 * @file
 * Page base type
 */
 
abstract class pageroute_page {
  var $title;
  var $options;
  var $route;
  var $name;
  var $weight;
  
  abstract public function get_form(&$form_state, &$args);
  abstract protected function set_up();
  
  public function pageroute_page($page) {
    $this->load($page);
  }
  
  public function load($page) {
    $this->title = $page->title;
    $this->options = $page->options;
    $this->route = &$page->route;
    $this->name = $page->name;
    $this->type = $page->type;
    $this->weight = $page->weight;
  }
  
  public function get_cancel_target() {
    return PAGEROUTE_CURRENT;
  }
  
  public static function get_object($page_data) {
    $type = $page_data->type;
    $bases = pageroute_get_types('base');   
    
    // Include needed type!
    include_once(drupal_get_path('module', $bases[$type]) .'/'. $bases[$type] .'.page_'. $page_data->type .'.inc');
  
    $page_class = $bases[$type] .'_page_'. $page_data->type;
    
    $page = new $page_class($page_data);
    
    $page->set_up();
  
    return $page;
  }
  
  /*
   * These form fields are quite common for node forms, so page
   * types, which are also displaying node forms can use it
   * @param $delete Wheter the "delete checkbox" should be added
   */
  public static function node_ui($page, &$form, $delete = TRUE) {
    $form['options']['preview'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display preview button'),
      '#default_value' => isset($page->options['preview']) ? $page->options['preview'] : 0,
      '#weight' => 5,        
    );
    $form['options']['submit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display submit button'),
      '#default_value' => isset($page->options['submit']) ? $page->options['submit'] : 1,
      '#weight' => 5,        
    );
    if ($delete) {
      $form['options']['nodelete'] = array(
        '#type' => 'checkbox',
        '#title' => t('Never display the delete button'),
        '#default_value' => isset($page->options['nodelete']) ? $page->options['nodelete'] : 1,
        '#weight' => 5,        
      );
    }
  }

  protected function configure_form(&$form) {
    if (isset($this->options['preview']) && !$this->options['preview']) {
      unset($form['buttons']['preview']);
    }

    if (isset($this->options['submit']) && !$this->options['submit']) {
      unset($form['buttons']['submit']);
    }

    if (isset($this->options['delete']) && !$this->options['delete']) {
      unset($form['buttons']['delete']);
    }
  }
  
  public static function form_submitted(&$form_state) { }
}
