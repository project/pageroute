<?php

/**
 * @file
 * Page edit type
 */

include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page.inc');
include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page_add.inc');

class pageroute_page_edit extends pageroute_page {

  function get_form(&$form_state, &$args) {

      $args['hide_pageroute_buttons'] = FALSE;
      $args['submit_action'] = PAGEROUTE_FORWARD;
      return pageroute_page_edit::get_node_edit_form($form_state, $this, $args);
  }
      
  /*
   * Returns the node edit form for the configured node type or the give node id
   * 
   * @param $argument_index Tells the function, which route argument shall be used for getting the node id.
   *   This allows reusing this function from other pages, e.g. the node management page. If nothing is
   *   given the function will use $nid page argument
   */
  public static function get_node_edit_form(&$form_state, &$page, &$args = NULL) {    

    ($args && isset($args['nid'])) ? $nid = $args['nid'] : $nid = $page->options['nid'];
        
    if (empty($nid) && $page->options['content-type']) {
      return pageroute_page_add::get_node_add_form($form_state, $page);
    }
    else if (!is_numeric($nid) || !($node = node_load($nid)) ) {        
      drupal_not_found();
      pageroute_exit_now();
    }
    
    ($page->options['content-type']) ? $type = $page->options['content-type'] : $type = $node->type;

    if ($args['todo']['action'] == t('Delete')) {
      $args['hide_pageroute_buttons'] = TRUE;
      $args['submit_action'] = PAGEROUTE_CURRENT;      
      return pageroute_node_delete_confirm($node, $form, $page, $args);
    }

    if (node_access('update', $node)) {
      //load the node edit form    
      unset($form_state['node']);
      $form = drupal_retrieve_form($type .'_node_form', $form_state, $node);
      drupal_prepare_form($type .'_node_form', $form, $form_state);

      $page->configure_form(&$form);

      return $form;
    }
    else {
      drupal_access_denied();
      pageroute_exit_now();
    }
  }

  public static function ui($page, &$form) {
    
    $form['options']['content-type'] = array(
      '#type' => 'select',
      '#title' => t('Content type for new nodes'),
      '#options' => array('' => '') + node_get_types('names'),
      '#default_value' => $page->options['content-type'],
      '#weight' => 2,
      '#description' => t('If there is no node id in the URL, a node add form '.
                          'for this content-type will be displayed.'.
                          'Leave it empty to show the Page Not Found error instead.'),
    );

    pageroute_page::node_ui($page, &$form, TRUE);
  }

  public static function help() {
    return t('A page of this type will present a common node editing form '.
             'of a configurable content-type. It will '.
             'edit the node with the id taken from the first argument of '.
             'the pageroute. Furthermore this type can be configured to show a '.
             'node adding form of a specific content-type if the node id argument '.
             'is missing. So you can build a '.
             'pageroute that manages the creation and editing of nodes of '.
             'the same type.');
  }
  
  public static function info() {
    return array('name' => t('Node editing form'));
  }
  
  public static function form_submitted(&$form_state) {
  
    $todo = NULL;
  
    if ($form_state['clicked_button']['#value'] == t('Delete')) {
        if (is_numeric($form_state['clicked_button']['#name'])) {
          $target = $form_state['clicked_button']['#name'];
        }
        else {
          $target = $form_state['storage']['args']['todo']['target'];
        }
        $todo = array('action' => $form_state['clicked_button']['#value'], 'target' => $target);
    }
    
    return $todo;
  }
  
  public function get_cancel_target() {
    return PAGEROUTE_FORWARD;
  }
  
  protected function set_up() {
    include_once(drupal_get_path('module', 'node') .'/node.pages.inc');
  }
}

/*
 * Provide an extra delete page to keep control about the destination parameter.
 */
function pageroute_node_delete_confirm($node, $form, &$page, $args = NULL) {
  if (node_access('delete', $node)) {

    $form = array();
    $form['nid'] = array('#type' => 'value', '#value' => $node->nid);

    $form = confirm_form($form,
      t('Are you sure you want to delete %title?', array('%title' => $node->title)),
      pageroute_get_page_path($page, $args),
      t('This action cannot be undone.'), t('Delete'), t('Cancel')
    );
        
    $form['actions']['submit']['#submit'][] = 'pageroute_node_delete_confirm_submit';

    return $form;
  }
  drupal_access_denied();
  pageroute_exit_now();
}

function pageroute_node_delete_confirm_submit($form, &$form_state) {
  $form_state['target'] = PAGEROUTE_CURRENT;

  if ($form_state['values']['confirm']) {
    node_delete($form_state['values']['nid']);
  }
  
  $form_state['rebuild'] = TRUE;
  unset($form_state['values']['target']);
  unset($form_state['button_clicked']);
  unset($form_state['args']['todo']);
  unset($form_state['todo']);
}
