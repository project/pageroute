<?php

/**
 * @file
 * Page user edit type
 */

include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page.inc');

class pageroute_page_user_edit extends pageroute_page {  

  public function get_form(&$form_state, &$args) {
    $form = array();
   
    if ($args && isset($args['uid'])) {
      $account = user_load(array('uid' => $args['uid']));
    }
    else {    
      $account = user_load(array('uid' => pageroute_page_get_uid($this, 'administer users')));
    }
    
    if (!$account->uid) {
      drupal_not_found();
      pageroute_exit_now();
    }

    if ($args['todo']['action'] == t('Delete')) {
      $args['hide_pageroute_buttons'] = TRUE;
      $args['submit_action'] = PAGEROUTE_CURRENT;
      return pageroute_node_delete_confirm($account, $form, $page, $args);
    }
    
    $args['hide_pageroute_buttons'] = FALSE;
    $args['submit_action'] = PAGEROUTE_FORWARD;
        
    $form = drupal_retrieve_form('user_profile_form', &$form_state, $account, $this->options['category']);
    drupal_prepare_form('user_edit', &$form, &$form_state);

    $page->configure_form(&$form);
    
    return $form;
  }

  public static function ui($page, &$form) {
    $categories = array();
    foreach (_user_categories(false) as $category) {
      $categories[$category['name']] = $category['title'];
    }
    $form['options']['category'] = array(
      '#type' => 'select',
      '#title' => t('Category'),
      '#description' => t('The form of the chosen category will be used for this page.'),
      '#required' => TRUE,
      '#default_value' => isset($page->options['category']) ? $page->options['category'] : '',
      '#weight' => 2,
      '#options' => $categories,
    );
  }

  public static function help() {
    return t('A page of this type will present users\' account editing page '.
      'inside a pageroute. By giving a user id as second pageroute argument '.
      'it\'s also possible for administrators to edit another users\' account.'. 
      'By choosing another category it could also be used to integrate a drupal user profile into a pageroute.') .
      '<br />'.
      t('!warn This page type is still experimental. If you consider using it, make '.
      'sure you \'ve read !link before!', 
      array('!warn' => '<strong>'. t('Warning!') .'</strong>', '!link' => l(t('this issue'), 'http://drupal.org/node/128045')));   
  }
  
  public static function info() {
    return array('name' => t('User editing form'));
  }
  
  public static function form_submitted(&$form_state) {
  
    $todo = NULL;
  
    if ($form_state['clicked_button']['#value'] == t('Delete')) {
        if (is_numeric($form_state['clicked_button']['#name'])) {
          $target = $form_state['clicked_button']['#name'];
        }
        else {
          $target = $form_state['storage']['args']['todo']['target'];
        }
        $todo = array('action' => $form_state['clicked_button']['#value'], 'target' => $target);
    }
    
    return $todo;
  }
  
  protected function set_up() {
    include_once(drupal_get_path('module', 'user') .'/user.pages.inc');
  }
}

/*
 * Provide an extra delete page to keep control about the destination parameter.
 */
function pageroute_user_delete_confirm($user, $form, &$page, $args = NULL) {
  if (user_access('delete', $user)) {

    $form = array();
    $form['uid'] = array('#type' => 'value', '#value' => $user->uid);

    $form = confirm_form($form,
      t('Are you sure you want to delete %name?', array('%name' => $user->name)),
      pageroute_get_page_path($page, $args),
      t('This action cannot be undone.'), t('Delete'), t('Cancel')
    );
    
    $form['actions']['submit']['#submit'][] = 'pageroute_user_delete_confirm_submit';

    return $form;
  }
  drupal_access_denied();
  pageroute_exit_now();
}

function pageroute_user_delete_confirm_submit($form, &$form_state) {
  $form_state['target'] = PAGEROUTE_CURRENT;

  if ($form_state['values']['confirm']) {
    user_delete($form_state['values']['uid']);
  }
  
  $form_state['rebuild'] = TRUE;
  unset($form_state['values']['target']);
  unset($form_state['button_clicked']);
  unset($form_state['args']['todo']);
  unset($form_state['todo']);
}
