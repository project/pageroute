<?php

/**
 * @file
 * Page add type
 */

include_once(drupal_get_path('module', 'pageroute') .'/pageroute.page.inc');

class pageroute_page_add extends pageroute_page {  
  /*
   * Returns the node adding form for the configured node type
   */
  function get_form(&$form_state, &$args) {
    
    $form = array();

    $args['hide_pageroute_buttons'] = FALSE;
    $args['submit_action'] = PAGEROUTE_FORWARD;
    
    return pageroute_page_add::get_node_add_form(&$form_state, $this);
  }
  
  public static function get_node_add_form(&$form_state, &$page) {
    $type = $page->options['content-type'];

    // If a node type has been specified, validate its existence.
    if (node_access('create', $type)) {
      $account = user_load(array('uid' => pageroute_page_get_uid($this, 'administer nodes')));

      // Initialize settings:
      $node = array('uid' => $account->uid, 'name' => $account->name, 'type' => $type);
      
      unset($form_state['node']);
      $form = drupal_retrieve_form($type .'_node_form', $form_state, $node);
      drupal_prepare_form($type .'_node_form', $form, $form_state);

      $page->configure_form(&$form);
      
      return $form;
    }
    else {
      drupal_access_denied();
      pageroute_exit_now();
    }
  }

  public static function ui($page, &$form) {
    $form['options']['content-type'] = array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#options' => node_get_types('names'),
      '#required' => TRUE,
      '#default_value' => $page->options['content-type'],
      '#weight' => 2,
    );
    if (!isset($page->options['cancel'])) {
      //we default to show the cancel button on add node forms
      $form['options']['cancel']['#default_value'] = t('Cancel');
    }
    pageroute_page::node_ui($page, $form, FALSE);
  }

  public static function help() {
    return t('A page of this type will present a common node adding form '.
             'of a configurable content-type. The id of the new node will be '.
             'added as new argument to the current path, so that other pages '.
             'like a node view page can make use of it.');
  }
  
  public static function info() {
    return array('name' => t('Node adding form'));
  }
  
  public function get_cancel_target() {
    return PAGEROUTE_FORWARD;
  }
  
  public static function form_submitted(&$form_state) {
  
    switch ($form_state['clicked_button']['#value']) {
      case t('Preview'):
        $form_state['storage']['args']['submit_action'] = PAGEROUTE_CURRENT;
        break;
    }
  }
  
  protected function set_up() {
    include_once(drupal_get_path('module', 'node') .'/node.pages.inc');
  }
}
